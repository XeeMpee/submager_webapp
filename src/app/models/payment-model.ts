export class PaymentModel {
  constructor(
    public key: string = '',
    public invoiceUrl: string = '',
    public owner: string = '',
    public payTo: string = '',
    public price: number = 0,
    public status: boolean = false,
    public sourceId: string = null,
    public type: string = '') {
  }
}
