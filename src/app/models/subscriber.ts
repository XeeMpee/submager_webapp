export class Subscriber {
  constructor(
    public name: string = '',
    public logoUrl: string =  '',
    public contractedAt: string = '',
    public canceledAt: string = ''
  ) {
  }
}
