import {PaymentModel} from './payment-model';
import {Subscriber} from './subscriber';

export class Subscription extends PaymentModel {
  public subscriber: Subscriber;

  constructor(key: string = '',
              invoiceUrl: string = '',
              owner: string = '',
              payTo: string = '',
              price: number = 0,
              status: boolean = false,
              sourceId: string = '',
              payment: string = '') {
    super(key, invoiceUrl, owner, payTo, price, status, sourceId, payment);
  }
}
