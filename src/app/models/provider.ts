export class Provider {
  constructor(
    public name: string = '',
    public logoUrl: string =  '',
    public contractedAt: string = '',
    public canceledAt: string = ''
  ) {
  }
}
