import {PaymentModel} from './payment-model';
import {Provider} from './provider';

export class Recurring extends PaymentModel {
  public provider: Provider;

  constructor(key: string = '',
              invoiceUrl: string = '',
              owner: string = '',
              payTo: string = '',
              price: number = 0,
              status: boolean = false,
              sourceId: string = '',
              payment: string = '') {
    super(key, invoiceUrl, owner, payTo, price, status, sourceId, payment);
  }
}
