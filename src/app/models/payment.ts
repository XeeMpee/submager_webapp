export interface Payment {
  key: string;
  invoiceUrl: string;
  owner: string;
  payTo: string;
  price: number;
  sourceId: string;
  status: boolean;
  type: 'subscription'|'recurring';
}
