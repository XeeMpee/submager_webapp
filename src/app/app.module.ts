import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {SubscriptionComponent} from './components/subscription/subscription.component';
import {RecurringComponent} from './components/recurring/recurring.component';
import {OptymizatorComponent} from './components/optymizator/optymizator.component';
import {NavComponent} from './components/shared/nav/nav.component';
import {PaymentsComponent} from './components/shared/payments/payments.component';
import {WidgetComponent} from './components/shared/payments/widget/widget.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {SubscriptionWidgetComponent} from './components/shared/subscription/subscription-widget/subscription-widget.component';
import {SignInComponent} from './components/sign-in/sign-in.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {AuthService} from './services/auth.service';
import { RecurringWidgetComponent } from './components/shared/recurring/recurring-widget/recurring-widget.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxBootstrapConfirmModule} from 'ngx-bootstrap-confirm';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SubscriptionComponent,
    RecurringComponent,
    OptymizatorComponent,
    NavComponent,
    PaymentsComponent,
    WidgetComponent,
    SubscriptionWidgetComponent,
    SignInComponent,
    SignUpComponent,
    RecurringWidgetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxBootstrapConfirmModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
