import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {Payment} from '../models/payment';
import {Subscription} from '../models/subscription';
import {Recurring} from '../models/recurring';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private db: AngularFirestore) {
  }

  private path = '/payments';

  static createPayment(type: string, payment: Payment): Subscription | Recurring {
    switch (type) {
      case 'subscription':
        return new Subscription(
          payment.key,
          payment.invoiceUrl,
          payment.owner,
          payment.payTo,
          payment.price,
          payment.status,
          payment.sourceId,
          payment.type);
      case 'recurring':
        return new Recurring(
          payment.key,
          payment.invoiceUrl,
          payment.owner,
          payment.payTo,
          payment.price,
          payment.status,
          payment.sourceId,
          payment.type);
      default:
        throw new Error('Provider type does not exist');
    }
  }

  get(paymentId: string): Observable<any> {
    return this.db.doc(`${this.path}/${paymentId}`).valueChanges();
  }

  getAllPayments(owner: string = '', type: string = 'subscription'): Observable<(Subscription | Recurring)[]> {
    return this.db.collection(this.path, x => {
      if (type === 'all') {
        return x;
      }
      if (type === 'recurring') {
        return x.where('owner', '==', owner).where('type', '==', type).where('status', '==', false);
      }
      return x.where('owner', '==', owner).where('type', '==', type);
    }).valueChanges({idField: 'key'})
      .pipe(
        map(payments => {

          return payments.map((payment: Payment) => {
            return DataService.createPayment(payment.type, payment);
          });
        })
      );
  }

  getProvider(id: string, provider: string = 'subscription'): Observable<any> {
    const collection = provider === 'subscription' ? '/subscribers' : 'providers';
    return this.db.doc(`${collection}/${id}`).valueChanges({idField: 'key'});
  }

  update(id: string, collection: string, data: any): Promise<void> {
    console.log(id, collection, data);
    return this.db.doc(`/${collection}/${id}`).update(data);
  }
}
