import { Component, OnInit } from '@angular/core';
import {Subscription} from '../../models/subscription';
import {DataService} from '../../services/data.service';
import {Subscriber} from '../../models/subscriber';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {

  subscriptions: Subscription[];

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    const user = JSON.parse(localStorage.getItem('user'));

    this.dataService.getAllPayments(user.uid, 'subscription').subscribe((data: Subscription[]) => {
      this.subscriptions = data;
      this.subscriptions.map(subscription => {
        this.dataService.getProvider(subscription.sourceId).subscribe(provider => {
          subscription.subscriber = new Subscriber(
            provider.name,
            provider.logoUrl,
            provider.contractedAt,
            provider.canceledAt,
          );
        });
      });
    });
  }
}
