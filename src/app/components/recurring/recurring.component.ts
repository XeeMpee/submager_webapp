import {Component, OnInit} from '@angular/core';
import {Subscriber} from '../../models/subscriber';
import {DataService} from '../../services/data.service';
import {Router} from '@angular/router';
import {Subscription} from '../../models/subscription';
import {Recurring} from '../../models/recurring';
import {Provider} from '../../models/provider';

@Component({
  selector: 'app-recurring',
  templateUrl: './recurring.component.html',
  styleUrls: ['./recurring.component.scss']
})
export class RecurringComponent implements OnInit {

  recurings: Recurring[];

  constructor(private dataService: DataService, private router: Router) {
  }

  ngOnInit(): void {
    const user = JSON.parse(localStorage.getItem('user'));

    this.dataService.getAllPayments(user.uid, 'recurring').subscribe((data: Recurring[]) => {
      this.recurings = data;
      this.recurings.filter(recurring => !recurring.status);
      this.recurings.map(recurring => {

        this.dataService.getProvider(recurring.sourceId, 'recurring').subscribe(provider => {
          recurring.provider = new Provider(
            provider.name,
            provider.logoUrl,
            provider.contractedAt,
            provider.canceledAt,
          );
        });
      });
    });
  }

}
