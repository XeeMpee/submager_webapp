import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from '../../../../models/subscription';
import {ToastrService} from 'ngx-toastr';
import {NgxBootstrapConfirmService} from 'ngx-bootstrap-confirm';
import {DataService} from '../../../../services/data.service';

@Component({
  selector: 'app-subscription-widget',
  templateUrl: './subscription-widget.component.html',
  styleUrls: ['./subscription-widget.component.scss']
})
export class SubscriptionWidgetComponent {

  @Input() subscription: Subscription;
  @Input() toggle = true;

  options = {
    title: 'Czy na pewno chcesz wykonać tą akcję',
    confirmLabel: 'Tak',
    declineLabel: 'Nie'
  };

  constructor(private toastrService: ToastrService,
              private ngxBootstrapConfirmService: NgxBootstrapConfirmService,
              private dataService: DataService) {
  }

  cancel(): void {
    this.ngxBootstrapConfirmService.confirm(this.options).then((res: boolean) => {
      if (res && this.subscription.status) {
        this.toastrService.success('Subskrybcja została anulowana', 'Wypowiedzenie subskrybcji');
      }

      if (res && !this.subscription.status) {
        this.toastrService.success('Subskrybcja została wznowiona', 'Wznowienie subskrybcji');
      }
      this.subscription.status = !this.subscription.status;
      const data = {...this.subscription};
      delete data.subscriber;
      delete data.key;
      this.dataService.update(this.subscription.key, 'payments', data).then(() => {});
    });
  }
}
