import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionWidgetComponent } from './subscription-widget.component';

describe('SubscriptionWidgetComponent', () => {
  let component: SubscriptionWidgetComponent;
  let fixture: ComponentFixture<SubscriptionWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscriptionWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
