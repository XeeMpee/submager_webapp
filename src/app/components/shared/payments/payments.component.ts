import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  payments = {
    title: 'Przelewy',
    tiles: [
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Przelew na konto'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Przelew własny'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Przelew na telefon'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Zeskanuj i zapłać'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Zlecenia stałe'
      }
    ]
  };

  mobile = {
    title: 'Płatności mobilne',
    tiles: [
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Kod BLIK'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Zakupy bez kodu BLIK'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Czeki BLIK'
      },
      {
        icon: 'fas fa-bus-alt',
        link: '',
        text: 'Bilety komunikacyjne'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Opłaty parkingowe'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Opłaty za autostrady'
      }
    ]
  };

  different = {
    title: 'Inne operacje',
    tiles: [
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Prośba o przelew'
      },
      {
        icon: 'far fa-money-bill-alt',
        link: '',
        text: 'Utwórz kod QR'
      }
    ]
  };

  constructor(
    public authService: AuthService
  ) {
  }

  ngOnInit(): void {
  }

}
