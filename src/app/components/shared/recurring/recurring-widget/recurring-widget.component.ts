import {Component, Input, OnInit} from '@angular/core';
import {Recurring} from '../../../../models/recurring';
import {ToastrService} from 'ngx-toastr';
import {DataService} from '../../../../services/data.service';

@Component({
  selector: 'app-recurring-widget',
  templateUrl: './recurring-widget.component.html',
  styleUrls: ['./recurring-widget.component.scss']
})
export class RecurringWidgetComponent {
  @Input() subscription: Recurring;
  @Input() toggle = true;

  constructor(private toastrService: ToastrService, private dataService: DataService) {
  }

  pay(): void {
    if (!this.subscription.status) {
      this.subscription.status = !this.subscription.status;
      const data = {...this.subscription};
      delete data.provider;
      delete data.key;
      this.dataService.update(this.subscription.key, 'payments', data).then(() => {
        this.toastrService.success('Rachunek został opłacony', 'Zlecenie przelewu');
      });
    }
  }
}
