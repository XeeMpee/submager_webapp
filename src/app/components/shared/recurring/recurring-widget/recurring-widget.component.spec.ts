import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecurringWidgetComponent } from './recurring-widget.component';

describe('RecurringWidgetComponent', () => {
  let component: RecurringWidgetComponent;
  let fixture: ComponentFixture<RecurringWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecurringWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecurringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
