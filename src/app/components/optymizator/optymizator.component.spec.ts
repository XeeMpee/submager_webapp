import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptymizatorComponent } from './optymizator.component';

describe('OptymizatorComponent', () => {
  let component: OptymizatorComponent;
  let fixture: ComponentFixture<OptymizatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptymizatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptymizatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
