import {Component, OnInit} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Subscription} from '../../models/subscription';
import {Subscriber} from '../../models/subscriber';
import {Router} from '@angular/router';
import {Recurring} from '../../models/recurring';
import {Provider} from '../../models/provider';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  payments: (Subscription | Recurring)[];

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    const user = JSON.parse(localStorage.getItem('user'));

    this.dataService.getAllPayments(user.uid, 'all').subscribe((data: (Subscription | Recurring)[]) => {
      this.payments = data;
      this.payments.map(subscription => {
        this.dataService.getProvider(subscription.sourceId, subscription.type).subscribe(provider => {
          if (subscription instanceof Subscription) {
            subscription.subscriber = new Subscriber(
              provider.name,
              provider.logoUrl,
              provider.contractedAt,
              provider.canceledAt,
            );
          }

          if (subscription instanceof Recurring) {
            subscription.provider = new Provider(
              provider.name,
              provider.logoUrl,
              provider.contractedAt,
              provider.canceledAt,
            );
          }
        });
      });
    });
  }

}
